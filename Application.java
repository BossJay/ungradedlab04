import java.util.Scanner;

public class Application
{
	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("input amountStudied");
		int amountStudied = scan.nextInt();
	
		Student jp = new Student();
		jp.setName("Einstein");
		jp.setSubject("Math");
		jp.setStudentID(1);

		
		Student john = new Student();
		john.setName("John");
		john.setSubject("Science");
		john.setStudentID(2);
    
		/*
		System.out.println(john.name + ", " + john.subject + ", " + john.studentID);
		System.out.println(jp.name + ", " + jp.subject + ", " + jp.studentID);
		*/
    
		jp.studentID();
		jp.yourSubj();
		john.studentID();
		john.yourSubj();
		
		Student[] section4 = new Student[3];
		
		section4[0] = jp;
		section4[1] = john;
		
		section4[2] = new Student();
		section4[2].setName("Bella");
		section4[2].setSubject("History");
		section4[2].setStudentID(3);
		
		for(int i = 0;i < 2; i++)
		{
		section4[2].learn(amountStudied);
		}
    
    System.out.println(section4[0].getSubject());
    System.out.println(section4[0].getName());
    System.out.println(section4[0].getStudentID());		
    
		System.out.println(section4[0].getSubject());
    System.out.println(section4[1].getName());
    System.out.println(section4[2].getStudentID());
    
    /*
		System.out.println(section4[0].name + ", " + section4[0].subject + ", " + section4[0].studentID + ", " + section4[0].amountLearnt);
		System.out.println(section4[1].name + ", " + section4[1].subject + ", " + section4[1].studentID + ", " + section4[1].amountLearnt);
		System.out.println(section4[2].name + ", " + section4[2].subject + ", " + section4[2].studentID + ", " + section4[2].amountLearnt);
	  */

    /*
		System.out.println(section4[1].amountLearnt);
    section4[0].learn(3);
		section4[1].learn(-3);
		System.out.println(section4[1].amountLearnt);
    */
    
	  /*
		section4[2].studentID();
		section4[2].yourSubj();
	  */
	}
}
